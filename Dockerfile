FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install apache2 apt-utils -y
EXPOSE 80
CMD apachectl -D FOREGROUND